# Research Question - Group 10

Members:	Jayakrishna Brundavanam Radha, Pearl Doherty Kyokwebaza, Akshith Dharma Reddy Bureddy, Chenchurao Gurram, Bansaribahen Ahir

Topic:		Performance of machine learning algorithms for face detection in IT organisations
 
RQ: 		What is the most accurate algorithm for face detection in IT organisations?

Context:	Performance of algorithms in IT organisations

Population:	Employee's of the IT organisation

Intervention:	Software programming language (java/javascript/Python3.7)

Comparison:	MobileNet, KNN, SVM 

Outcome:	Most accurate algorithm (low training loss)
